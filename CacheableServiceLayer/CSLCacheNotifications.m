//
//  CSLCacheNotifications.m
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 25/07/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import "CSLCacheNotifications.h"
#import <SurfObjcUtils/SurfObjcUtils.h>

#define kCSLCacheNotification @"CSLCacheUpdatedNotification"
#define kCSLCacheAspectsNotificationKey @"updatedServiceAspects"
#define kCSLUserInfoNotificationKey @"userInfo"

@implementation CSLCacheNotifications

+(void)postWithAspects:(NSArray<NSString *> *)aspects {
    [self postWithAspects:aspects userInfo:nil];
}

+ (void)postWithAspects:(NSArray<NSString *> *)aspects userInfo:(id)userInfo {
    if (!aspects || !aspects.count) return;
    NSMutableDictionary *info = [NSMutableDictionary new];
    info[kCSLCacheAspectsNotificationKey] = aspects;
    if (userInfo)
        info[kCSLUserInfoNotificationKey] = userInfo;
    [[NSNotificationCenter defaultCenter] postNotificationName:kCSLCacheNotification
                                                        object:nil
                                                      userInfo:info];
}

+ (void)subscribeToAspects:(NSArray<NSString *> *)aspects untilObjectDies:(id)object handler:(void (^)(NSArray<NSString *> *, id))handler {
    [[NSNotificationCenter defaultCenter] srf_observeName:kCSLCacheNotification object:nil untilObjectDies:object handler:^(NSNotification *notif) {
        NSArray *realAspects = notif.userInfo[kCSLCacheAspectsNotificationKey];
        for (NSString *aspect in realAspects)
            if ([aspects containsObject:aspect]) {
                handler(realAspects, notif.userInfo[kCSLUserInfoNotificationKey]);
                return;
            }
    }];
}

@end
