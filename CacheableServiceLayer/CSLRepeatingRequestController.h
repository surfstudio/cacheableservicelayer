//
//  CSLRepeatingRequestController.h
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 18/01/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CacheableServiceLayer/CSLRequest.h>

/**
 A convenience class wrapping a reusable logic usually performed in view controllers, displaying a refreshable info.
 
 Feature 1. Storing state info:
 
 - the request which is currently happening;
 
 - the last successful response;
 
 - the flag whether the last request happened and failed.
 
 Use -startRequest:completion: method to use this feature automatically.
 
 Feature 2. Tracking changes in the displayed data. Define observedDataMapper, returning the data you actually display
 which will be compared with the previous data with -isEqual: method.
 In every completion handler, you'll get a flag indicating wheather the data actually changed.
 
 Feature 3. Subscription to cache update notification. You can specify a fabric block,
 creating a new cache-only request every time when the given aspects are changed.
 If any changes are registered in the results (processed by observedDataMapper), changeHandler block will be called.
 
 */
@interface CSLRepeatingRequestController : NSObject

/// Configure this mapper to return the data you actually displays.
/// The result will be compared with -isEqual: method to check whether actual changes happened.
@property (nonatomic, copy) id (^observedDataMapper)(CSLResponse *response);

/// The current request, being performed with -startRequest:completion: method.
@property (nonatomic, readonly) CSLRequest *currentRequest;

/// The last successful response.
@property (nonatomic, readonly) CSLResponse *lastSuccessfulResponse;

/// The last response, including unsuccessful ones
@property (nonatomic, readonly) CSLResponse *lastResponse;

/// Subscribes to the given cache aspects changes. After every notification,
/// a new request is created with requestMaker (you'd better call -configureToCheckChangesSynchronously for it),
/// and the result is compared with the previous one, processed by observedDataMapper.
/// If actual changes happened, changeHandler is called (you'll typically reload your UI there).
- (void)subscribeToCacheAspects:(NSArray <NSString *>*)aspects
              cacheRequestMaker:(CSLRequest *(^)())requestMaker
                  changeHandler:(void (^)(CSLResponse *response))handler;

/// Calls the given request, tracking its state and results.
- (void)startRequest:(CSLRequest *)request completion:(void (^)(CSLResponse *response, BOOL changed))completion;

@end
