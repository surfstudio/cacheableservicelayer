//
//  CSLRequest.m
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 21/12/15.
//  Copyright © 2015 Surf Studio. All rights reserved.
//

#import "CSLRequest.h"
#import <objc/runtime.h>
#import <SurfObjcUtils/SurfObjcUtils.h>
#import "CSLManager.h"
#import "NSError+CSLError.h"
#import "CSLDelegate.h"

@interface CSLRequest ()
@property (strong, nonatomic) CSLResponse *response;
@property (strong, nonatomic) SRFCompletionContext *taskContext;
@end

@implementation CSLRequest

- (instancetype)init {
    self = [super init];
    if (self) {
        if ([[CSLManager sharedInstance].delegate respondsToSelector:@selector(csl_requestWasCreated:)])
            [[CSLManager sharedInstance].delegate csl_requestWasCreated:self];
    }
    return self;
}

- (instancetype)configureToCheckChangesSynchronously {
    _canCompleteSynchronously = YES;
    if ([[CSLManager sharedInstance].delegate respondsToSelector:@selector(csl_requestWasConfiguredToCheckChangesSynchronously:)])
        [[CSLManager sharedInstance].delegate csl_requestWasConfiguredToCheckChangesSynchronously:self];
    return self;
}

- (void)performCustomScenarioWithCompletion:(CSLRequestCompletion)completion {
    [self doesNotRecognizeSelector:_cmd];
}

- (void)performWithCompletion:(CSLRequestCompletion)completion {
    if (!completion)
        completion = ^(CSLResponse *response){};
    if (_state != CSLRequestStateCreated) {
        completion([[CSLResponse new] addError:[NSError csl_errorWithCode:CSLErrorCodeAlreadyCalled]]);
        return;
    }
    //oragnize notifying delegate and switching state
    if ([[CSLManager sharedInstance].delegate respondsToSelector:@selector(csl_requestWillStart:)])
        [[CSLManager sharedInstance].delegate csl_requestWillStart:self];
    _state = CSLRequestStatePerforming;
    SRFCompletionContext *context = [SRFCompletionContext contextWithCompletion:^{
        _state = CSLRequestStateFinished;
        if ([[CSLManager sharedInstance].delegate respondsToSelector:@selector(csl_requestDidFinish:)])
            [[CSLManager sharedInstance].delegate csl_requestDidFinish:self];
        completion(self.response);
    }];
    self.taskContext = context;
    
    //track when the current event cycle is over
    __block BOOL theSameCycle = YES;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        theSameCycle = NO;
    });
    
    //call the actual request performing
    [self performCustomScenarioWithCompletion:^(CSLResponse *response) {
        //define the completion as closing the context with the given response
        void (^completion)() = ^{
            [context completeWithBlock:^{
                self.response = response;
            }];
        };
        //call the completion synchronously or with delay
        if (theSameCycle && !self.canCompleteSynchronously && response.error.code != CSLErrorCodeCancelled)
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), completion);
        else
            completion();
    }];
}

- (void)cancelWithResponse:(CSLResponse *)response {
    [self.taskContext completeWithBlock:^{
        self.response = response;
    }];
}

- (void)cancel {
    [self cancelWithResponse:[[CSLResponse new] addError:[NSError csl_errorWithCode:CSLErrorCodeCancelled]]];
}

@end
