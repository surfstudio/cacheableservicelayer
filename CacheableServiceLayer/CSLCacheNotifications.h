//
//  CSLCacheNotifications.h
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 25/07/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CSLCacheNotifications : NSObject

/// Posts notification that the specified cache aspects were updated.
/// Should be called on main thread, when the updates are already saved, but before completing the request.
+ (void)postWithAspects:(NSArray<NSString *> *)aspects userInfo:(id)userInfo;

/// Calls -postWithAspects:userInfo: with nil userInfo.
+ (void)postWithAspects:(NSArray<NSString *> *)aspects;

/// Subscribes to cache changes until the given object dies.
/// The handler is called when one or more of the given cache aspects are changed.
+ (void)subscribeToAspects:(NSArray <NSString *> *)aspects
           untilObjectDies:(id)object
                   handler:(void (^)(NSArray <NSString *> *aspects, id userInfo))handler;

@end
