//
//  CSLManager.h
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 13/01/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CSLDelegate;

/**
 Singleton that stores CacheableServiceLayer preferences.
 You can configure it in -application:didFinishLaunchingWithOptions:
 */
@interface CSLManager : NSObject

/// Specify the delegate to be notified about service layer events.
@property (weak, nonatomic) id<CSLDelegate> delegate;

/// Singleton instance of CSLManager.
+ (instancetype)sharedInstance;

@end
