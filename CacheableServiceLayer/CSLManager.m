//
//  CSLManager.m
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 13/01/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import "CSLManager.h"

@interface CSLManager ()
@end

@implementation CSLManager

+ (instancetype)sharedInstance {
    static id res = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        res = [[self alloc] init];
    });
    return res;
}

@end
