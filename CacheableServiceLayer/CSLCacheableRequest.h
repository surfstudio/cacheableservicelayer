//
//  CSLCacheableRequest.h
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 25/01/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import <CacheableServiceLayer/CSLRequest.h>

typedef NS_ENUM(NSInteger, CSLCacheScenario) {
    CSLCacheScenarioServerOnly,
    CSLCacheScenarioCacheOnly,
    CSLCacheScenarioFirstCacheIfFailedThenServer,
    CSLCacheScenarioFirstCacheThenRefreshFromServer,
    CSLCacheScenarioFirstServerWithTimeoutThenCache,
};
/**
 Abstract class for requests, that can restore the response from cache.
 */
@interface CSLCacheableRequest : CSLRequest

/// Cache scenario, used while performing the request.
@property (nonatomic) CSLCacheScenario cacheScenario;

/// Timeout used in scenario CSLCacheScenarioFirstServerWithTimeoutThenCache
@property (nonatomic) NSTimeInterval serverTimeoutBeforeCallingCache;

/// Abstract primitive method to call the server and update the cache.
- (void)callServerAndUpdateCacheWithCompletion:(CSLRequestCompletion)completion;

/// Abstract primitive method to extract the data from the cache.
- (CSLResponse *)cachedData;

@end
