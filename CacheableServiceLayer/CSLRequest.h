//
//  CSLRequest.h
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 21/12/15.
//  Copyright © 2015 Surf Studio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CacheableServiceLayer/CSLResponse.h>

typedef NS_ENUM(NSInteger, CSLRequestState) {
    CSLRequestStateCreated,
    CSLRequestStatePerforming,
    CSLRequestStateFinished,
};

/**
 CSLRequest is an abstract base class for all requests to the service.
 It can cobmine other serviceRequests, or call the server directly, or call the cache.
 Anyway, you'll receive CSLResponse (or one if its subclasses) as a result.
 */
@interface CSLRequest : NSObject

/// A user-defined object to extend CSLRequest with project-specific stored properties.
@property (nonatomic) id userInfo;

/// If request is allowed to complete synchronously within -perform... method. Default is NO.
@property (nonatomic) BOOL canCompleteSynchronously;

/// Current state of the request
@property (nonatomic, readonly) CSLRequestState state;

/// The received request's response. Nil if not finished yet.
@property (nonatomic, readonly) CSLResponse *response;

- (instancetype)init;

/// Convenience method to configure ignoreErrors=YES, canCompleteSynchronously=YES, cacheScanario=cacheOnly,
/// and return self.
- (instancetype)configureToCheckChangesSynchronously;

/// Use this method to perform the request. Wraps -performCustomScenarioWithCompletion:,
/// handling states and protecting from synchronous completion if needed.
- (void)performWithCompletion:(CSLRequestCompletion)completion;

/// Abstract primitive method to perform the actual request. Called by -performWithCompletion:,
/// wrapped by handling states and sync callbacks. Don't call this method directly.
- (void)performCustomScenarioWithCompletion:(CSLRequestCompletion)completion;

/// Cancels the request, calling completion synchronously with a failed response with errorCancelled.
- (void)cancel;

/// Cancels the request, calling completion synchronously with the given response.
- (void)cancelWithResponse:(CSLResponse *)response;

@end
