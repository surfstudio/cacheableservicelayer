//
//  NSError+CSLError.m
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 21/12/15.
//  Copyright © 2015 Surf Studio. All rights reserved.
//

#import "NSError+CSLError.h"

@implementation NSError (CSLError)

+ (instancetype)csl_errorWithCode:(CSLErrorCode)code {
    return [self csl_errorWithCode:code underlyingError:nil];
}

+ (instancetype)csl_errorWithCode:(CSLErrorCode)code underlyingError:(NSError *)error {
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[NSLocalizedDescriptionKey] = ^id{
        switch (code) {
            case CSLErrorCodeUnknown: default:      return NSLocalizedString(@"CSLErrorDescription unknown", nil);
            case CSLErrorCodeNotCached:             return NSLocalizedString(@"CSLErrorDescription notCached", nil);
            case CSLErrorCodeInvalidResponse:       return NSLocalizedString(@"CSLErrorDescription invalidResponse", nil);
            case CSLErrorCodeCancelled:             return NSLocalizedString(@"CSLErrorDescription cancelled", nil);
            case CSLErrorCodeAlreadyCalled:         return NSLocalizedString(@"CSLErrorDescription alreadyCalled", nil);
            case CSLErrorCodeNetworkError:          return NSLocalizedString(@"CSLErrorDescription networkError", nil);
            case CSLErrorCodeUnknownCacheScenario:  return NSLocalizedString(@"CSLErrorDescription unknownCacheScenario", nil);
        }
    }();
    if (error)
        params[NSUnderlyingErrorKey] = error;
    return [NSError errorWithDomain:CSLErrorDomain
                               code:code
                           userInfo:params];
}

+ (instancetype)csl_notCachedError {
    return [self csl_errorWithCode:CSLErrorCodeNotCached];
}

+ (instancetype)csl_invalidResponseError {
    return [self csl_errorWithCode:CSLErrorCodeInvalidResponse];
}

@end
