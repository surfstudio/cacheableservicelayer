//
//  CacheableServiceLayer.h
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 18/07/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for CacheableServiceLayer.
FOUNDATION_EXPORT double CacheableServiceLayerVersionNumber;

//! Project version string for CacheableServiceLayer.
FOUNDATION_EXPORT const unsigned char CacheableServiceLayerVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CacheableServiceLayer/PublicHeader.h>


#import <CacheableServiceLayer/CSLManager.h>
#import <CacheableServiceLayer/CSLDelegate.h>
#import <CacheableServiceLayer/CSLCacheNotifications.h>
#import <CacheableServiceLayer/CSLCacheableRequest.h>
#import <CacheableServiceLayer/CSLRequest.h>
#import <CacheableServiceLayer/CSLResponse.h>
#import <CacheableServiceLayer/NSError+CSLError.h>
#import <CacheableServiceLayer/CSLRepeatingRequestController.h>
