//
//  NSError+CSLError.h
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 21/12/15.
//  Copyright © 2015 Surf Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSString *const CSLErrorDomain = @"com.surf.CacheableServiceLayer";

typedef NS_ENUM(NSInteger, CSLErrorCode) {
    CSLErrorCodeUnknown,
    CSLErrorCodeNotCached,
    CSLErrorCodeInvalidResponse,
    CSLErrorCodeCancelled,
    CSLErrorCodeAlreadyCalled,
    CSLErrorCodeNetworkError,
    CSLErrorCodeUnknownCacheScenario,
};

/**
 Convenience error creation methods for CSLErrorDomain.
 */
@interface NSError (CSLError)

/// Error with CSLErrorDomain and the given code.
+ (instancetype)csl_errorWithCode:(CSLErrorCode)code;

/// Error with CSLErrorDomain, the given code, and underlying error.
+ (instancetype)csl_errorWithCode:(CSLErrorCode)code underlyingError:(NSError *)error;

/// Error with CSLErrorDomain and CSLErrorCodeNotCached code.
+ (instancetype)csl_notCachedError;

/// Error with CSLErrorDomain and CSLErrorCodeInvalidResponse code.
+ (instancetype)csl_invalidResponseError;
@end
