//
//  CSLResponse.m
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 21/12/15.
//  Copyright © 2015 Surf Studio. All rights reserved.
//

#import "CSLResponse.h"
#import "NSError+CSLError.h"

@implementation CSLResponse

- (BOOL)success {
    return !_error;
}

- (instancetype)addError:(NSError *)error {
    self.error = error;
    return self;
}

+ (instancetype)responseWithNetworkError:(NSError *)error {
    CSLResponse *res = [[self alloc] init];
    if (error)
        res.error = ([error.domain isEqualToString:CSLErrorDomain]?
                     error:
                     [NSError csl_errorWithCode:CSLErrorCodeNetworkError
                                underlyingError:error]);
    return res;
}

@end
