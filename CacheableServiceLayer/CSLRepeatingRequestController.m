//
//  CSLRepeatingRequestController.m
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 18/01/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import "CSLRepeatingRequestController.h"
#import <libextobjc/extobjc.h>
#import "CSLCacheNotifications.h"

@interface CSLRepeatingRequestController ()
@property (nonatomic) CSLRequest *currentRequest;
@property (nonatomic) CSLResponse *lastSuccessfulResponse;
@property (nonatomic) id observedInfo;
@end

@implementation CSLRepeatingRequestController

- (void)subscribeToCacheAspects:(NSArray<NSString *> *)aspects cacheRequestMaker:(CSLRequest *(^)())requestMaker changeHandler:(void (^)(CSLResponse *))handler {
    @weakify(self);
    [CSLCacheNotifications subscribeToAspects:aspects untilObjectDies:self handler:^(NSArray<NSString *> *aspects, id userInfo) {
        @strongify(self);
        if (self.currentRequest) return;
        [requestMaker() performWithCompletion:^(CSLResponse *response) {
            if (!response.success) return;
            if ([self setLastSuccessfulResponseAndCheckIfChanged:response])
                handler(response);
        }];
    }];
}

- (void)startRequest:(CSLRequest *)request completion:(void (^)(CSLResponse *, BOOL))completion {
    _currentRequest = request;
    [request performWithCompletion:^(CSLResponse *response) {
        BOOL changed = NO;
        if (response.success)
            changed = [self setLastSuccessfulResponseAndCheckIfChanged:response];
        _currentRequest = nil;
        _lastResponse = response;
        completion(response, changed);
    }];
}

- (BOOL)setLastSuccessfulResponseAndCheckIfChanged:(CSLResponse *)response {
    _lastSuccessfulResponse = response;
    if (!_observedDataMapper) return YES;
    id observedInfo = _observedDataMapper(response);
    BOOL equals = observedInfo ? [observedInfo isEqual:_observedInfo] : observedInfo == _observedInfo;
    _observedInfo = observedInfo;
    return !equals;
}

@end
