//
//  CSLCacheableRequest.m
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 25/01/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import "CSLCacheableRequest.h"
#import "CSLManager.h"
#import "NSError+CSLError.h"
#import "CSLDelegate.h"

@implementation CSLCacheableRequest

- (instancetype)init {
    self = [super init];
    if (self) {
        _cacheScenario = CSLCacheScenarioFirstCacheIfFailedThenServer;
    }
    return self;
}

- (instancetype)configureToCheckChangesSynchronously {
    _cacheScenario = CSLCacheScenarioCacheOnly;
    return [super configureToCheckChangesSynchronously];
}

- (CSLResponse *)cachedData {
    [self doesNotRecognizeSelector:_cmd];
    return nil;
}

- (void)callServerAndUpdateCacheWithCompletion:(CSLRequestCompletion)completion {
    [self doesNotRecognizeSelector:_cmd];
}

- (void)performServerScenarioWithCompletion:(CSLRequestCompletion)completion {
    [self callServerAndUpdateCacheWithCompletion:completion];
}

- (void)processUnhandledResponse:(CSLResponse *)response {
    if ([[CSLManager sharedInstance].delegate respondsToSelector:@selector(csl_request:didReceiveUnhandledResponse:)])
        [[CSLManager sharedInstance].delegate csl_request:self didReceiveUnhandledResponse:response];
}

- (void)performCustomScenarioWithCompletion:(CSLRequestCompletion)completion {
    switch (self.cacheScenario) {
        case CSLCacheScenarioCacheOnly:
            completion([self cachedData]);
            break;
        case CSLCacheScenarioServerOnly: {
            [self performServerScenarioWithCompletion:completion];
            break; }
        case CSLCacheScenarioFirstCacheIfFailedThenServer: {
            CSLResponse *cached = [self cachedData];
            if (cached.success)
                completion(cached);
            else
                [self performServerScenarioWithCompletion:completion];
            break; }
        case CSLCacheScenarioFirstCacheThenRefreshFromServer: {
            CSLResponse *cached = [self cachedData];
            __block void (^myCompletion)(CSLResponse *) = completion;
            if (cached.success) {
                myCompletion(cached);
                myCompletion = ^(CSLResponse *response){
                    [self processUnhandledResponse:response];
                };
            }
            [self performServerScenarioWithCompletion:myCompletion];
            break; }
        case CSLCacheScenarioFirstServerWithTimeoutThenCache: {
            __block BOOL completed = NO;
            id mutex = [[NSObject alloc] init];
            //this branch will call completion anyway (if not called already)
            [self performServerScenarioWithCompletion:^(CSLResponse *response) {
                @synchronized(mutex) {
                    if (completed) {
                        [self processUnhandledResponse:response];
                        return;
                    }
                    if (response.success)
                        completion(response);
                    else {
                        //if server not replies, try to get cached data
                        CSLResponse *cached = [self cachedData];
                        if (cached.success)
                            completion(cached);
                        else
                            //if no data cached, return the original failed response
                            completion(response);
                    }
                    completed = YES;
                }
            }];
            //this branch will call completion only if cached data exists and the request isn't completed yet
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(self.serverTimeoutBeforeCallingCache * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                @synchronized(mutex) {
                    if (completed) return;
                    CSLResponse *cached = [self cachedData];
                    if (cached.success) {
                        completion(cached);
                        completed = YES;
                    }
                }
            });
            break; }
        default:
            completion([[CSLResponse new] addError:[NSError csl_errorWithCode:CSLErrorCodeUnknownCacheScenario]]);
            break;
    }
}

@end
