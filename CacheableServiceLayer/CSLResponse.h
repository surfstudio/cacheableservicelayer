//
//  CSLResponse.h
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 21/12/15.
//  Copyright © 2015 Surf Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 Class wrapping the results of any CSLRequest.
 Error property should indicate the success of the request.
 You can store the actual result in result property,
 or create a request-specific CSLResponse subclass with custom properties.
 */
@interface CSLResponse : NSObject

/// Error that happened during the request. Should be nil if the request is successful, non-nil otherwise.
@property (nonatomic) NSError *error;

/// A convenience read-only flag indicating the absence of error.
@property (nonatomic, readonly) BOOL success;

/// Can store the actual result of the request.
@property (nonatomic) id result;

/// A user-defined object to extend CSLResponse with project-specific stored properties.
@property (nonatomic) id userInfo;

/// Creates an instance of CSLResponse with the given error,
/// wrapping it with CSLErrorCodeNetworkError if its domain isn't CSLErrorDomain already.
+ (instancetype)responseWithNetworkError:(NSError *)error;

/// Assigns the error of the receiver and returns the receiver. Convenience method.
- (instancetype)addError:(NSError *)error;

@end

typedef void (^CSLRequestCompletion)(CSLResponse *response);
