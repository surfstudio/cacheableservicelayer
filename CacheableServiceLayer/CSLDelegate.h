//
//  CSLDelegate.h
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 12/08/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CSLRequest, CSLResponse;

/**
 Implement this protocol and assign it to CSLManager delegate to be notified about service layer events.
 */
@protocol CSLDelegate <NSObject>
@optional

/// Called when CSLRequest is created
- (void)csl_requestWasCreated:(CSLRequest *)request;

/// Called when CSLRequest is called, before switching its state to performing.
- (void)csl_requestWillStart:(CSLRequest *)request;

/// Called when CSLRequest is finished, after assigning its response, after switching its state to finished,
/// and before calling completion.
- (void)csl_requestDidFinish:(CSLRequest *)request;

/// Called when server request is finished, but the containing service request is already completed.
/// It happens in some cache scenarios (firstCacheThenRefreshFromServer, firstServerWithTimeoutThenCache).
- (void)csl_request:(CSLRequest *)request didReceiveUnhandledResponse:(CSLResponse *)response;

/// Called when CSLRequest performs -configureToCheckChangesSynchronously: method.
/// Use it to change the request's config appropriately.
- (void)csl_requestWasConfiguredToCheckChangesSynchronously:(CSLRequest *)request;

@end
