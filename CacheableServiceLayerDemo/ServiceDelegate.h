//
//  ServiceDelegate.h
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 15/08/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CacheableServiceLayer/CacheableServiceLayer.h>

@interface ServiceDelegate : NSObject <CSLDelegate>
+ (instancetype)sharedInstance;
@end
