//
//  BookListCell.h
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 29/12/15.
//  Copyright © 2015 Surf Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Book;

@interface BookListCell : UITableViewCell
@property (nonatomic) Book *book;
@property (nonatomic, copy) void (^tapHandler)(Book *book);
@end
