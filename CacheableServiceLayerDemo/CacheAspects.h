//
//  CacheAspects.h
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 25/07/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

///Operations with books
static NSString *const BooksAspect = @"books";
