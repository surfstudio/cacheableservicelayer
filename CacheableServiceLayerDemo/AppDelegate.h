//
//  AppDelegate.h
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 21/12/15.
//  Copyright © 2015 Surf Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic) UINavigationController *navController;
+ (instancetype)sharedInstance;
@end
