//
//  ServiceRequestConfig.h
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 12/08/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CacheableServiceLayer/CacheableServiceLayer.h>
#import "ServerRequest.h"

@interface ServiceRequestConfig : NSObject
///ViewController to show error message. Default is nil (showing in the current VC).
@property (nonatomic) UIViewController *vcToShowError;

/// Flag to ignore showing any errors
@property (nonatomic) BOOL ignoreServerErrors;

/// Specify this to handle errors manually
@property (strong, nonatomic) void (^customErrorHandler)(CSLRequest *request, CSLResponse *response);

@property (strong, nonatomic) ServerRequest *serverRequest;

@end

@interface CSLRequest (ConcreteConfig)
@property (nonatomic, readonly) ServiceRequestConfig *config;
@end
