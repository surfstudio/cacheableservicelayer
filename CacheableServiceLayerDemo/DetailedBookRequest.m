//
//  DetailedBookRequest.m
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 11/01/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import "DetailedBookRequest.h"
#import "RequestUtils.h"

#define cacheTimeout 3*24*60*60 //3 days

@implementation DetailedBookRequest

- (ServerRequest *)createServerRequest {
    return [ServerRequest requestWithMethod:GET relativeUrl:[NSString stringWithFormat:@"books/%@.json",_bookId] parameters:nil];
}

- (CSLResponse *)cachedData {
    CSLResponse *response = [[CSLResponse alloc] init];
    if (self.serverRequest.loadTimestamp)
        response.result = [Book MR_findFirstByAttribute:@"id_" withValue:_bookId];
    else
        response.error = [NSError csl_notCachedError];
    return response;
}

- (void)callServerAndUpdateCacheWithCompletion:(CSLRequestCompletion)completion {
    [self.serverRequest performWithCompletion:^(ServerResponse *serverResponse) {
        CSLResponse *response = [CSLResponse responseWithServerResponse:serverResponse];
        if (response.success) {
            NSDictionary *bookDict = serverResponse.dict;
            if (bookDict && [bookDict isKindOfClass:[NSDictionary class]] && [bookDict[@"id"] isEqualToString:_bookId]) {
                [MagicalRecord serialSaveWithBlock:^(NSManagedObjectContext *context) {
                    Book *book = [Book MR_importFromObject:bookDict inContext:context];
                    RequestTimestamp *stamp = [self.serverRequest saveTimestampWithPeriod:cacheTimeout
                                                                                toContext:context];
                    [stamp addBooksObject:book];
                } completion:^(BOOL contextDidSave, NSError *error) {
                    [CSLCacheNotifications postWithAspects:@[BooksAspect]];
                    completion([[self.cachedData addServerResponse:serverResponse] addError:error]);
                }];
            } else
                completion([response addError:[NSError csl_invalidResponseError]]);
        } else
            completion(response);
    }];
}

@end
