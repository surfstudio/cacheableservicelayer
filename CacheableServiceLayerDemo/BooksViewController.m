//
//  BooksViewController.m
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 29/12/15.
//  Copyright © 2015 Surf Studio. All rights reserved.
//

#import "BooksViewController.h"
#import "AllBooksRequest.h"
#import <BlocksKit/A2DynamicDelegate.h>
#import <BlocksKit/BlocksKit+UIKit.h>
#import "NSObject+CallMethodsWithPrefix.h"
#import <libextobjc/extobjc.h>
#import "BookListCell.h"
#import <SurfObjcUtils/SurfObjcUtils+UIKit.h>
#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>
#import "A2DynamicDelegate+ManyMethods.h"
#import "BookDetailsViewController.h"
#import "ServerRequest.h"

@interface BooksViewController ()
@property (nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) IBOutlet UIView *hudView;
@property (nonatomic) IBOutlet UIView *noBooksView;
@property (nonatomic) IBOutlet UIView *cannotLoadView;
@property (nonatomic) IBOutlet UILabel *cannotLoadLabel;
@property (nonatomic) CSLRepeatingRequestController *requestController;
@property (nonatomic) UIView *currentView;
@property (nonatomic, readonly) NSArray *books;
@end

@implementation BooksViewController

- (void)viewDidLoad {
    [self callMethodsWithPrefix:@"VDL_"];
    [self reloadAllForced:NO];
}

- (void)VDL_configureRequestController {
    @weakify(self);
    _requestController = [CSLRepeatingRequestController new];
    [_requestController setObservedDataMapper:^id(CSLResponse *response) {
        return [response.result bk_map:^id(Book *book) {
            return @[book.id_,
                     unnullify(book.name),
                     unnullify(book.author)];
        }];
    }];
    [_requestController subscribeToCacheAspects:@[BooksAspect] cacheRequestMaker:^CSLRequest *{
        return [[AllBooksRequest new] configureToCheckChangesSynchronously];
    } changeHandler:^(CSLResponse *response) {
        @strongify(self);
        [self refreshUiWithTableReloading:YES];
    }];
}

- (void)VDL_configreRefreshControl {
    _tableView.srf_attachedRefreshControl = [[UIRefreshControl alloc] init];
    @weakify(self);
    [_tableView.srf_attachedRefreshControl bk_addEventHandler:^(id sender) {
        @strongify(self);
        [self reloadAllForced:YES];
    } forControlEvents:UIControlEventValueChanged];
}

- (void)VDL_configureTableView {
    _tableView.rowHeight = UITableViewAutomaticDimension;
    _tableView.estimatedRowHeight = 79;
    _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero]; //to remove separators for empty cells
    
    NSString *reuseIdentifier = @"cell";
    [_tableView registerNib:[UINib nibWithNibName:@"BookListCell" bundle:nil] forCellReuseIdentifier:reuseIdentifier];
    
    //UITableViewDataSource
    @weakify(self);
    _tableView.dataSource =
    [_tableView attachImplementation:@protocol(UITableViewDataSource) methods:
     @selector(tableView:numberOfRowsInSection:), ^(id t, NSInteger section){
         @strongify(self);
         return (NSInteger)self.books.count;
     },
     @selector(tableView:cellForRowAtIndexPath:), ^UITableViewCell *(id t, NSIndexPath *indexPath){
         @strongify(self);
         BookListCell *cell = [self.tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
         cell.book = self.books[indexPath.row];
         [cell setTapHandler:^(Book *book) {
             @strongify(self);
             BookDetailsViewController *vc = [BookDetailsViewController new];
             vc.bookId = book.id_;
             vc.title = book.name;
             vc.ephemeralRequests = NO;
             [self.navigationController pushViewController:vc animated:YES];
         }];
         return cell;
     }, nil];
}

- (void)VDL_configureEmptyStubs {
    @weakify(self);
    
    //DZNEmptyDataSetSource
    _tableView.emptyDataSetSource =
    [_tableView attachImplementation:@protocol(DZNEmptyDataSetSource) methods:
     @selector(customViewForEmptyDataSet:), ^UIView *(id s){
         @strongify(self);
         return self.currentView;
     }, nil];
    
    //DZNEmptyDataSetDelegate
    _tableView.emptyDataSetDelegate =
    [_tableView attachImplementation:@protocol(DZNEmptyDataSetDelegate) methods:
     @selector(emptyDataSetShouldAllowScroll:), ^(id s){
         return YES;
     }, nil];
}

- (NSArray *)books {
    return _requestController.lastSuccessfulResponse.result;
}

- (void)refreshUiWithTableReloading:(BOOL)reloadTable {
    UIView *oldCurrentView = _currentView;
    _currentView = ^UIView *{
        if (self.books) {
            if (self.books.count)
                return _tableView;
            else
                return _noBooksView;
        } else {
            if (_requestController.lastResponse)
                return _cannotLoadView;
            else
                return _hudView;
        }
    }();
    if (reloadTable)
        [_tableView reloadData];
    else if (oldCurrentView != _currentView)
        [_tableView reloadEmptyDataSet];
    
    BOOL unscrollableHud = _currentView == _hudView;
    _tableView.bounces = !unscrollableHud;
    _tableView.srf_attachedRefreshControl.srf_disabledInScrollView = unscrollableHud;
    _tableView.srf_attachedRefreshControl.refreshing = _requestController.currentRequest && !unscrollableHud;
}

- (void)reloadAllForced:(BOOL)forced {
    if (_requestController.currentRequest) return;
    CSLCacheableRequest *request = [AllBooksRequest new];
    request.config.vcToShowError = self;
    @weakify(self);
    [request.config setCustomErrorHandler:^(CSLRequest *request, CSLResponse *response) {
        @strongify(self);
        if (self.requestController.currentRequest && !self.books) {
            self.cannotLoadLabel.text = [ErrorMessages messageForRequest:request response:response];
            [self.cannotLoadLabel layoutIfNeeded];
        } else
            [ErrorMessages showErrorForRequest:request response:response];
    }];
    request.cacheScenario = (forced ?
                             CSLCacheScenarioServerOnly:
                             CSLCacheScenarioFirstCacheThenRefreshFromServer);
    [_requestController startRequest:request completion:^(CSLResponse *response, BOOL changed) {
        [self refreshUiWithTableReloading:changed];
    }];
    [self refreshUiWithTableReloading:NO];
}

@end
