//
//  RandomFloatRequest.m
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 28/12/15.
//  Copyright © 2015 Surf Studio. All rights reserved.
//

#import "RandomFloatRequest.h"
#import "RequestUtils.h"

@implementation RandomFloatRequest

- (void)performCustomScenarioWithCompletion:(CSLRequestCompletion)completion {
    ServerRequest *serverRequest = [ServerRequest requestWithMethod:GET relativeUrl:@"random.json" parameters:nil];
    [serverRequest performWithCompletion:^(ServerResponse *serverResponse) {
        RandomFloatResponse *response = [RandomFloatResponse responseWithServerResponse:serverResponse];
        if (response.success) response.error = ^NSError *{
            NSNumber *randomNum = serverResponse.dict[@"random"];
            if (!(randomNum && [randomNum isKindOfClass:[NSNumber class]]))
                return [NSError csl_invalidResponseError];
            float random = [randomNum floatValue];
            if (random < 0 || random > 1)
                return [NSError csl_invalidResponseError];
            response.randomFloat = random;
            return nil;
        }();
        completion(response);
    }];
}

@end

@implementation RandomFloatResponse
@end
