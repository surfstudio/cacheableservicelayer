//
//  NSManagedObjectContext+TempChild.m
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 04/07/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import "NSManagedObjectContext+TempChild.h"
#import <SurfObjcUtils/SurfObjcUtils.h>

@implementation NSManagedObjectContext (TempChild)

- (void)makeTempChildWithHolder:(id)holder block:(void (^)(NSManagedObjectContext *))block {
    __block NSManagedObjectContext *childContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    childContext.parentContext = self;
    [childContext performBlockAndWait:^{
        if (block)
            block(childContext);
    }];
    [holder srf_addDeallocationHandler:^{
        childContext = nil;
    }];
}

- (void)makeTempChildWithHolder:(id)holder block:(void (^)(NSManagedObjectContext *))block completion:(void (^)())completion {
    __block NSManagedObjectContext *childContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    childContext.parentContext = self;
    [childContext performBlock:^{
        if (block)
            block(childContext);
        dispatch_async(dispatch_get_main_queue(), ^{
            completion();
        });
    }];
    [holder srf_addDeallocationHandler:^{
        childContext = nil;
    }];
}

@end
