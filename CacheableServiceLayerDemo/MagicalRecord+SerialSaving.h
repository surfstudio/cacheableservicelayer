//
//  MagicalRecord+SerialSaving.h
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 15/02/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import <MagicalRecord/MagicalRecord.h>

@interface MagicalRecord (SerialSaving)
+ (void)serialSaveWithBlock:(void (^)(NSManagedObjectContext *))block completion:(MRSaveCompletionHandler)completion;
@end
