//
//  Book+CoreDataProperties.m
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 20/02/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Book+CoreDataProperties.h"

@implementation Book (CoreDataProperties)

@dynamic author;
@dynamic bookDescription;
@dynamic id_;
@dynamic name;
@dynamic sortOrder;
@dynamic requests;

@end
