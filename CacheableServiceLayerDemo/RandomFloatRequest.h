//
//  RandomFloatRequest.h
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 28/12/15.
//  Copyright © 2015 Surf Studio. All rights reserved.
//

#import <CacheableServiceLayer/CacheableServiceLayer.h>

///Request random float number. Returns RandomFloatResponse with randomFloat property.
@interface RandomFloatRequest : CSLRequest
@end

@interface RandomFloatResponse : CSLResponse
@property (nonatomic) float randomFloat;
@end
