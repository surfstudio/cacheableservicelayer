//
//  MessageDisplayManager.m
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 21/12/15.
//  Copyright © 2015 Surf Studio. All rights reserved.
//

#import "MessageDisplayManager.h"
#import <UIKit/UIKit.h>
#import <Masonry/Masonry.h>
#import "UserMessageView.h"
#import <SurfObjcUtils/SurfObjcUtils+UIKit.h>

@implementation MessageDisplayManager

+ (instancetype)sharedInstance {
    static id res = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        res = [[MessageDisplayManager alloc] init];
    });
    return res;
}

- (UIViewController*)currentVc {
    UIViewController *vc = [UIApplication sharedApplication].keyWindow.rootViewController;
    while (YES) {
        UIViewController *next = nil;
        if (vc.presentedViewController)
            next = vc.presentedViewController;
        else if ([vc isKindOfClass:[UITabBarController class]])
            next = ((UITabBarController*)vc).selectedViewController;
        else if ([vc isKindOfClass:[UINavigationController class]])
            next = ((UINavigationController*)vc).topViewController;
        if (next)
            vc = next;
        else
            break;
    }
    return vc;
}

- (void)showMessage:(NSString *)message inVc:(UIViewController *)vc mode:(MessageDisplayMode)mode {
    if (!vc) vc = [self currentVc];
    if (!vc) {
        NSLog(@"WARNING! Cannot find currentVc for showing messages");
        return;
    }
    UIView *container = vc.view;
    UserMessageView *plate = [UserMessageView view];
    [container addSubview:plate];
    [plate mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.and.trailing.and.top.equalTo(container);
        make.height.equalTo(@(plate.frame.size.height)).with.priority(999);
    }];
    plate.text = message;
    plate.mode = mode;
    plate.srf_hiddenWithHeightConstraint = YES;
    [plate layoutIfNeeded];
    [UIView animateWithDuration:0.5 animations:^{
        plate.srf_hiddenWithHeightConstraint = NO;
        [plate layoutIfNeeded];
    } completion:^(BOOL finished) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [UIView animateWithDuration:0.5 animations:^{
                plate.srf_hiddenWithHeightConstraint = YES;
                [plate layoutIfNeeded];
            } completion:^(BOOL finished) {
                [plate removeFromSuperview];
            }];
        });
    }];
}

@end
