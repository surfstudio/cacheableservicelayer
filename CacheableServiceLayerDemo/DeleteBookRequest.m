//
//  DeleteBookRequest.m
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 13/01/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import "DeleteBookRequest.h"
#import "RequestUtils.h"

@implementation DeleteBookRequest

- (void)performCustomScenarioWithCompletion:(CSLRequestCompletion)completion {
    ServerRequest *serverRequest = [ServerRequest requestWithMethod:DELETE relativeUrl:[NSString stringWithFormat:@"books/%@.json",_bookId] parameters:nil];
    [serverRequest performWithCompletion:^(ServerResponse *serverResponse) {
        CSLResponse *response = [CSLResponse responseWithServerResponse:serverResponse];
        [MagicalRecord serialSaveWithBlock:^(NSManagedObjectContext *context) {
            Book *book = [Book MR_findFirstByAttribute:@"id_" withValue:_bookId inContext:context];
            if (book)
                [book MR_deleteEntityInContext:context];
        } completion:^(BOOL contextDidSave, NSError *error) {
            [CSLCacheNotifications postWithAspects:@[BooksAspect]];
            completion([response addError:error]);
        }];
    }];
}

@end
