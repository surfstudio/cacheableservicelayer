//
//  MessageDisplayManager.h
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 21/12/15.
//  Copyright © 2015 Surf Studio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, MessageDisplayMode) {
    MessageDisplayModeError,
    MessageDisplayModeSuccess
};

@interface MessageDisplayManager : NSObject
+ (instancetype)sharedInstance;
- (void)showMessage:(NSString *)message inVc:(UIViewController *)vc mode:(MessageDisplayMode)mode;
@end
