//
//  ServerResponse.m
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 23/12/15.
//  Copyright © 2015 Surf Studio. All rights reserved.
//

#import "ServerResponse.h"

@implementation ServerResponse

- (NSString *)serverCode {
    return _dict[@"errorCode"];
}

- (NSString *)serverMessage {
    NSString *message = _dict[@"errorMessage"];
    if (message.length>4 && [message substringToIndex:4].intValue>0)
        message = [message substringFromIndex:5];
    return message;
}

@end
