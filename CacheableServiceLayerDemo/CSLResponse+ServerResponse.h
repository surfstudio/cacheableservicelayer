//
//  CSLResponse+ServerResponse.h
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 31/01/2017.
//  Copyright © 2017 Surf Studio. All rights reserved.
//

#import <CacheableServiceLayer/CacheableServiceLayer.h>
#import "ServerResponse.h"

@interface CSLResponse (ServerResponse)
@property (nonatomic) ServerResponse *serverResponse;
+ (instancetype)responseWithServerResponse:(ServerResponse *)serverResponse;
- (instancetype)addServerResponse:(ServerResponse *)serverResponse;
@end
