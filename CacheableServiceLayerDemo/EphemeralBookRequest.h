//
//  EphemeralBookRequest.h
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 04/07/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import "ServiceUtils.h"
#import "Book+CoreDataProperties.h"

// Returns an ephemeral Book object, not contained in the persistent model
@interface EphemeralBookRequest : CSLRequest
@property (nonatomic) NSString *bookId;
@end
