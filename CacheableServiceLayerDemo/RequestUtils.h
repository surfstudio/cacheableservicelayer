//
//  RequestUtils.h
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 25/07/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import <CacheableServiceLayer/CacheableServiceLayer.h>
#import "ServerRequest.h"
#import "CSLRequest+ServerRequest.h"
#import "ServerResponse.h"
#import "CSLResponse+ServerResponse.h"
#import "NSManagedObject+MRMappingUtils.h"
#import "MagicalRecord+SerialSaving.h"
#import "MagicalRecord+TempContext.h"
#import "RequestTimestamp.h"
