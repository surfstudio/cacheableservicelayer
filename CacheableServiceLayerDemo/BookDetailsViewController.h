//
//  BookDetailsViewController.h
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 25/01/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Book;

@interface BookDetailsViewController : UIViewController
@property (strong, nonatomic) NSString *bookId;
@property (nonatomic) BOOL ephemeralRequests;
@end
