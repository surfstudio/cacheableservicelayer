//
//  CSLResponse+ServerResponse.m
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 31/01/2017.
//  Copyright © 2017 Surf Studio. All rights reserved.
//

#import "CSLResponse+ServerResponse.h"
#import <SurfObjcUtils/SurfObjcUtils.h>

@implementation CSLResponse (ServerResponse)

- (ServerResponse *)serverResponse {
    return self.userInfo;
}

- (void)setServerResponse:(ServerResponse *)serverResponse {
    self.userInfo = serverResponse;
}

+ (instancetype)responseWithServerResponse:(ServerResponse *)serverResponse {
    CSLResponse *res = [self responseWithNetworkError:serverResponse.error];
    res.serverResponse = serverResponse;
    return res;
}

- (instancetype)addServerResponse:(ServerResponse *)serverResponse {
    self.serverResponse = serverResponse;
    return self;
}

@end
