//
//  ServerRequest.m
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 25/12/15.
//  Copyright © 2015 Surf Studio. All rights reserved.
//

#import "ServerRequest.h"
#import <AFNetworking/AFNetworking.h>
#import "ServerResponse.h"
#import "RequestTimestamp.h"
#import <MagicalRecord/MagicalRecord.h>
#import "NSPredicate+Templates.h"
#import <SurfObjcUtils/SurfObjcUtils.h>
#import "NSError+CSLError.h"
#import "ErrorMessages.h"
#import "ServiceRequestConfig.h"

#define baseUrl @"http://localhost/testapi"
#define requestTimeout 10

@interface NSObject (CollectionSortOrder)
- (void)addSortOrdersForKey:(NSString *)sortOrderKey;
@end

@interface ServerRequest ()
@property (strong, nonatomic) AFHTTPRequestOperation *operation;
@property (strong, nonatomic) ServerResponse *response;
@property (strong, nonatomic) SRFCompletionContext *taskContext;
@end

@implementation ServerRequest

+ (instancetype)requestWithMethod:(ServerRequestMethod)method relativeUrl:(NSString *)relativeUrl parameters:(NSDictionary *)parameters {
    ServerRequest *res = [[self alloc] init];
    res.method = method;
    res.relativeUrl = relativeUrl;
    res.parameters = parameters;
    return res;
}

- (void)performWithCompletion:(void (^)(ServerResponse *))completion {

//#warning DEBUG Additional delay
//    completion = ^(ServerResponse *response){
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            completion(response);
//        });
//    };
    
    SRFCompletionContext *context = [SRFCompletionContext contextWithCompletion:^{
        self.operation = nil;
        completion(self.response);
    }];
    self.taskContext = context;
    
    NSString *urlString = [baseUrl stringByAppendingPathComponent:_relativeUrl];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
    manager.requestSerializer.timeoutInterval = requestTimeout;
    manager.requestSerializer.HTTPMethodsEncodingParametersInURI = [NSSet setWithObjects:@"GET", @"POST", @"PUT", @"DELETE", nil];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingMutableContainers];
    id success = ^(AFHTTPRequestOperation * _Nonnull operation, NSDictionary *dict) {
        [context completeWithBlock:^{
            self.response = [ServerResponse new];
            self.response.httpResponse = operation.response;
            [dict addSortOrdersForKey:@"sortOrder"];
            self.response.dict = dict;
        }];
    };
    id failure = ^(AFHTTPRequestOperation * _Nonnull operation, NSError * _Nonnull error) {
        [context completeWithBlock:^{
            self.response = [ServerResponse new];
            self.response.httpResponse = operation.response;
            self.response.error = error;
        }];
    };
    self.operation = ^AFHTTPRequestOperation *{
        switch (_method) {
            case GET:   return [manager GET:    urlString parameters:_parameters success:success failure:failure];
            case POST:  return [manager POST:   urlString parameters:_parameters success:success failure:failure];
            case PUT:   return [manager PUT:    urlString parameters:_parameters success:success failure:failure];
            case DELETE:return [manager DELETE: urlString parameters:_parameters success:success failure:failure];
        }
    }();
}

- (void)cancel {
    [self.operation cancel];
    [self.taskContext completeWithBlock:^{
        self.response = [ServerResponse new];
        self.response.error = [NSError csl_errorWithCode:CSLErrorCodeCancelled];
    }];
}

#pragma mark - Timestamp encoding

- (NSString *)parametersString {
    if (!_parameters) return @"";
    return [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:_parameters
                                                                          options:NSJSONWritingPrettyPrinted
                                                                            error:nil]
                                 encoding:NSUTF8StringEncoding];
}

- (NSString *)methodString {
    switch (_method) {
        case GET:   return @"GET";
        case POST:  return @"POST";
        case PUT:   return @"PUT";
        case DELETE:return @"DELETE";
    }
    return nil;
}

- (NSPredicate *)timestampIdPredicate {
    return [NSPredicate predicateWithTemplate:@"(method == $METHOD) && (url == $URL) && (parameters == $PARAMS)"
                                       params:@{@"METHOD":[self methodString],
                                                @"URL":[self relativeUrl],
                                                @"PARAMS":[self parametersString]}];
}

- (RequestTimestamp *)saveTimestampWithPeriod:(NSTimeInterval)period toContext:(NSManagedObjectContext *)context {
    RequestTimestamp *stamp = [RequestTimestamp MR_findFirstWithPredicate:[self timestampIdPredicate]
                                                                inContext:context];
    if (!stamp)
        stamp = [RequestTimestamp MR_createEntityInContext:context];
    stamp.method = [self methodString];
    stamp.url = self.relativeUrl;
    stamp.parameters = [self parametersString];
    stamp.date = [NSDate date];
    stamp.expirationDate = [NSDate dateWithTimeIntervalSinceNow:period];
    return stamp;
}

- (RequestTimestamp *)loadTimestamp {
    RequestTimestamp *stamp = [RequestTimestamp MR_findFirstWithPredicate:[self timestampIdPredicate]];
    return (stamp && [stamp.expirationDate timeIntervalSinceNow] > 0)? stamp: nil;
}

@end


@implementation NSObject (CollectionSortOrder)

- (void)addSortOrdersForKey:(NSString *)sortOrderKey {
    if ([self isKindOfClass:[NSArray class]]) {
        [(NSArray *)self enumerateObjectsUsingBlock:^(id obj, NSUInteger i, BOOL *stop) {
            [obj addSortOrdersForKey:sortOrderKey];
            if ([obj isKindOfClass:[NSMutableDictionary class]] &&
                !obj[sortOrderKey]) {
                obj[sortOrderKey] = @(i);
            }
        }];
    } else if ([self isKindOfClass:[NSDictionary class]]) {
        [(NSDictionary *)self enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            [obj addSortOrdersForKey:sortOrderKey];
        }];
    }
}

@end
