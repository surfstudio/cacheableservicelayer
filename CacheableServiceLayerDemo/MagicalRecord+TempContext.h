//
//  MagicalRecord+TempContext.h
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 12/07/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import <MagicalRecord/MagicalRecord.h>

@interface MagicalRecord (TempContext)
+ (void)makeTempContextWithHolder:(id)holder block:(void (^)(NSManagedObjectContext *localContext))block;
+ (void)makeTempContextWithHolder:(id)holder block:(void (^)(NSManagedObjectContext *localContext))block completion:(void (^)())completion;
@end
