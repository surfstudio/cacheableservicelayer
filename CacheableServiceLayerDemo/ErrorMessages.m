//
//  ErrorMessages.m
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 12/08/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import "ErrorMessages.h"
#import "ServiceUtils.h"
#import "MessageDisplayManager.h"
#import "ServerRequest.h"
#import <AFNetworking/AFNetworking.h>
#import <AFNetworking/AFNetworkReachabilityManager.h>
#import "CSLResponse+ServerResponse.h"

@implementation ErrorMessages

+ (void)handleErrorForRequest:(CSLRequest *)request response:(CSLResponse *)response {
    if (response.error) {
        void (^customErrorHandler)(CSLRequest *, CSLResponse *) = request.config.customErrorHandler;
        if (customErrorHandler)
            customErrorHandler(request, response);
        else
            [self showErrorForRequest:request response:response];
    }
}

+ (void)showErrorForRequest:(CSLRequest *)request response:(CSLResponse *)response {
    if (request.config.ignoreServerErrors) return;
    [[MessageDisplayManager sharedInstance] showMessage:[self messageForRequest:request response:response]
                                                   inVc:request.config.vcToShowError
                                                   mode:MessageDisplayModeError];
}

+ (NSString *)messageForRequest:(CSLRequest *)request response:(CSLResponse *)response {
    ServerResponse *serverResponse = response.serverResponse;
    if (^BOOL {
        if (!serverResponse || !serverResponse.error) return NO;
        if (![AFNetworkReachabilityManager sharedManager].reachable) return YES;
        return serverResponse.httpResponse.statusCode == 0 && serverResponse.error.code < 0;
    }())
        return NSLocalizedString(@"errorNoInternetConnection", nil);
    switch (serverResponse.httpResponse.statusCode) {
        case 400: return NSLocalizedString(@"errorFailedToLoadData", nil);
        case 401: return NSLocalizedString(@"errorSessionExpired", nil);
        case 403: return NSLocalizedString(@"authNeedAuth", nil);
        default: return NSLocalizedString(@"errorFailedToLoadData",nil);
    }
}

@end
