//
//  DetailedBookRequest.h
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 11/01/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import "ServiceUtils.h"
#import "Book+CoreDataProperties.h"

///Detailed properties for the specific book. Returns Book.
@interface DetailedBookRequest : CSLCacheableRequest
@property (nonatomic) NSString *bookId;
@end
