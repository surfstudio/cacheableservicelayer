//
//  NSPredicate+TemplateUtil.m
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 20/02/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import "NSPredicate+Templates.h"

@implementation NSPredicate (Templates)

+ (instancetype)predicateWithTemplate:(NSString *)templateFormat params:(NSDictionary *)params {
    static NSMutableDictionary *templatesForFormats = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        templatesForFormats = [NSMutableDictionary new];
    });
    @synchronized(self) {
        NSPredicate *template = templatesForFormats[templateFormat];
        if (!template) {
            template = [NSPredicate predicateWithFormat:templateFormat];
            templatesForFormats[templateFormat] = template;
        }
        return [template predicateWithSubstitutionVariables:params?:@{}];
    }
}

@end
