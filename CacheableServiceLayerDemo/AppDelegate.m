//
//  AppDelegate.m
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 21/12/15.
//  Copyright © 2015 Surf Studio. All rights reserved.
//

#import "AppDelegate.h"
#import <MagicalRecord/MagicalRecord.h>
#import "BooksViewController.h"
#import "CleaningDeamon.h"
#import <SurfObjcUtils/SurfObjcUtils+CoreData.h>
#import "ServiceUtils.h"
#import "ServiceDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

+ (instancetype)sharedInstance {
    return (id)[UIApplication sharedApplication].delegate;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [MagicalRecord setupCoreDataStackWithAutoMigratingSqliteStoreNamed:@"Model"];
    [NSManagedObjectContext MR_rootSavingContext].srf_refCountingEnabled = YES;
    [[NSManagedObjectContext MR_rootSavingContext].srf_refCounter setDeleteObjectsHandler:^(NSSet<NSManagedObject *> *objects) {
        NSLog(@"Deleted objects: %@", objects);
    }];
    [CleaningDeamon start];
    
    [CSLManager sharedInstance].delegate = [ServiceDelegate sharedInstance];
    
    _navController = (id)_window.rootViewController;
    _navController.viewControllers = @[[[BooksViewController alloc] init]];
    return YES;
}

@end
