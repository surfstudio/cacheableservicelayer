//
//  A2DynamicDelegate+ManyMethods.h
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 15/01/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import <BlocksKit/A2DynamicDelegate.h>

@interface A2DynamicDelegate (ManyMethods)
- (instancetype)implementMethodsWithBlocks:(SEL)firstMethod, ...;
@end

@interface NSObject (AttachImplementation)
- (id)attachImplementation:(Protocol *)protocol methods:(SEL)firstMethod, ...;
@end
