//
//  NSPredicate+TemplateUtil.h
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 20/02/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSPredicate (Templates)

+ (instancetype)predicateWithTemplate:(NSString *)templateFormat params:(NSDictionary *)params;

@end
