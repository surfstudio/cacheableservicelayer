//
//  CSLRequest+ServerRequest.m
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 31/01/2017.
//  Copyright © 2017 Surf Studio. All rights reserved.
//

#import "CSLRequest+ServerRequest.h"
#import "ServiceRequestConfig.h"

@implementation CSLRequest (ServerRequest)

- (ServerRequest *)createServerRequest {
    [self doesNotRecognizeSelector:_cmd];
    return nil;
}

- (ServerRequest *)serverRequest {
    ServerRequest *res = self.config.serverRequest;
    if (!res) {
        res = [self createServerRequest];
        res.containingServiceRequest = self;
        self.config.serverRequest = res;
    }
    return res;
}

@end
