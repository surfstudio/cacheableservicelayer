//
//  RequestTimestamp+CoreDataProperties.m
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 20/02/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "RequestTimestamp+CoreDataProperties.h"

@implementation RequestTimestamp (CoreDataProperties)

@dynamic date;
@dynamic method;
@dynamic parameters;
@dynamic url;
@dynamic expirationDate;
@dynamic books;

@end
