//
//  ServerRequest.h
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 25/12/15.
//  Copyright © 2015 Surf Studio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServerResponse.h"
#import <CacheableServiceLayer/CacheableServiceLayer.h>

typedef enum{
    GET,
    POST,
    PUT,
    DELETE
} ServerRequestMethod;

@class RequestTimestamp;

@interface ServerRequest : NSObject
@property (nonatomic) ServerRequestMethod method;
@property (nonatomic) NSString *relativeUrl;
@property (nonatomic) NSDictionary *parameters;
@property (nonatomic, readonly) ServerResponse *response;
@property (weak, nonatomic) CSLRequest *containingServiceRequest;

+ (instancetype)requestWithMethod:(ServerRequestMethod)method
                      relativeUrl:(NSString *)relativeUrl
                       parameters:(NSDictionary *)parameters;
- (void)performWithCompletion:(void (^)(ServerResponse *serverResponse))completion;
- (void)cancel;

- (RequestTimestamp *)saveTimestampWithPeriod:(NSTimeInterval)period toContext:(NSManagedObjectContext *)context;
- (RequestTimestamp *)loadTimestamp;
@end
