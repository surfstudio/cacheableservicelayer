//
//  UserMessageView.m
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 21/12/15.
//  Copyright © 2015 Surf Studio. All rights reserved.
//

#import "UserMessageView.h"
#import <SurfObjcUtils/SurfObjcUtils+UIKit.h>

@interface UserMessageView()
@property (nonatomic) IBOutlet UILabel *label;
@property (nonatomic) IBOutlet UIImageView *warningSign;
@end

@implementation UserMessageView

+ (instancetype)view {
    return [[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil].firstObject;
}

- (void)setText:(NSString *)text {
    _text = text;
    _label.text = _text;
}

- (void)setMode:(MessageDisplayMode)mode {
    _mode = mode;
    self.backgroundColor = ^UIColor *{
        switch (mode) {
            case MessageDisplayModeError:    return [UIColor colorWithRed:87/255.0 green:86/255.0 blue:86/255.0 alpha:1];
            case MessageDisplayModeSuccess:  return [UIColor colorWithRed:80/255.0 green:158/255.0 blue:47/255.0 alpha:0.95];
            default: return nil;
        }
    }();
    self.warningSign.srf_hiddenWithWidthConstraint = mode != MessageDisplayModeError;
    [self layoutIfNeeded];
}

@end
