//
//  Book.h
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 23/12/15.
//  Copyright © 2015 Surf Studio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@class RequestTimestamp;

@interface Book : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "Book+CoreDataProperties.h"
