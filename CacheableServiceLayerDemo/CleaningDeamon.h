//
//  CleaningDeamon.h
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 28/03/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CleaningDeamon : NSObject
+ (void)start;
@end
