//
//  AllBooksRequest.m
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 23/12/15.
//  Copyright © 2015 Surf Studio. All rights reserved.
//

#import "AllBooksRequest.h"
#import "RequestUtils.h"

#define cacheTimeout 2*24*60*60 //2 days

@implementation AllBooksRequest

- (ServerRequest *)createServerRequest {
    return [ServerRequest requestWithMethod:GET relativeUrl:@"books.json" parameters:nil];
}

- (CSLResponse *)cachedData {
    CSLResponse *response = [[CSLResponse alloc] init];
    if (self.serverRequest.loadTimestamp)
        response.result = [Book MR_findAllSortedBy:@"sortOrder" ascending:YES];
    else
        response.error = [NSError csl_notCachedError];
    return response;
}

- (void)callServerAndUpdateCacheWithCompletion:(CSLRequestCompletion)completion {
    [self.serverRequest performWithCompletion:^(ServerResponse *serverResponse) {
        CSLResponse *response = [CSLResponse responseWithServerResponse:serverResponse];
        if (response.success) {
            NSArray *bookDicts = serverResponse.dict[@"books"];
            if (bookDicts && [bookDicts isKindOfClass:[NSArray class]]) {
                [MagicalRecord serialSaveWithBlock:^(NSManagedObjectContext *context) {
                    NSArray *books = [Book MR_importFromArray:bookDicts
                                         andRemoveOldIfNeeded:[Book MR_findAllInContext:context]
                                                    inContext:context];
                    RequestTimestamp *stamp = [self.serverRequest saveTimestampWithPeriod:cacheTimeout
                                                                                toContext:context];
                    [stamp addBooks:[NSSet setWithArray:books]];
                } completion:^(BOOL contextDidSave, NSError *error) {
                    [CSLCacheNotifications postWithAspects:@[BooksAspect]];
                    completion([[self.cachedData addServerResponse:serverResponse] addError:error]);
                }];
            } else
                completion([response addError:[NSError csl_invalidResponseError]]);
        } else
            completion(response);
    }];
}

@end
