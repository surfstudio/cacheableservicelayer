//
//  NSObject+CallMethodsWithPrefix.m
//  okey
//
//  Created by Dmitrii Trofimov on 28/10/15.
//  Copyright © 2015 SurfStudio. All rights reserved.
//

#import "NSObject+CallMethodsWithPrefix.h"
#import <objc/runtime.h>

@implementation NSObject (CallMethodsWithPrefix)

-(void)callMethodsWithPrefix:(NSString *)prefix {
    unsigned int methodCount = 0;
    Method *methods = class_copyMethodList([self class], &methodCount); //super methods are not enumerated here
    for (unsigned int i = 0; i<methodCount; i++) {
        Method method = methods[i];
        SEL selector = method_getName(method);
        if ([NSStringFromSelector(selector) hasPrefix:prefix])
            ((void (*)(id, SEL))[self methodForSelector:selector])(self, selector);
    }
    free(methods);
}

@end
