//
//  AllBooksRequest.h
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 23/12/15.
//  Copyright © 2015 Surf Studio. All rights reserved.
//

#import "ServiceUtils.h"
#import "Book+CoreDataProperties.h"

///Find all books. Returns NSArray <Book *>
@interface AllBooksRequest : CSLCacheableRequest
@end
