//
//  CSLRequest+ServerRequest.h
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 31/01/2017.
//  Copyright © 2017 Surf Studio. All rights reserved.
//

#import <CacheableServiceLayer/CacheableServiceLayer.h>
#import "ServerRequest.h"

@interface CSLRequest (ServerRequest)
@property (nonatomic, readonly) ServerRequest *serverRequest;
@end
