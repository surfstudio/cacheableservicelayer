//
//  RequestTimestamp.h
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 25/12/15.
//  Copyright © 2015 Surf Studio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@class Book;

@interface RequestTimestamp : NSManagedObject

+ (NSPredicate *)expiredPredicate;
+ (NSPredicate *)unexpiredPredicate;
+ (void)removeExpiredInContext:(NSManagedObjectContext *)context;

@end

NS_ASSUME_NONNULL_END

#import "RequestTimestamp+CoreDataProperties.h"
