//
//  CleaningDeamon.m
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 28/03/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import "CleaningDeamon.h"
#import <MagicalRecord/MagicalRecord.h>
#import "RequestTimestamp.h"
#import "MagicalRecord+SerialSaving.h"

@interface CleaningDeamon ()

@end

@implementation CleaningDeamon

+ (void)start {
    [self onTimer];
}

+ (void)onTimer {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(10 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [MagicalRecord serialSaveWithBlock:^(NSManagedObjectContext *context) {
            for (RequestTimestamp *stamp in [RequestTimestamp MR_findAllWithPredicate:[RequestTimestamp expiredPredicate]
                                                                            inContext:context]) {
                [stamp MR_deleteEntityInContext:context];
            }
        } completion:^(BOOL contextDidSave, NSError *error) {
            [self onTimer];
        }];
    });
}

@end
