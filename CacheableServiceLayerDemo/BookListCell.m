//
//  BookListCell.m
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 29/12/15.
//  Copyright © 2015 Surf Studio. All rights reserved.
//

#import "BookListCell.h"
#import "Book.h"

@interface BookListCell ()
@property (nonatomic) IBOutlet UILabel *authorLabel;
@property (nonatomic) IBOutlet UILabel *nameLabel;
@end

@implementation BookListCell

- (void)setBook:(Book *)book {
    _book = book;
    _authorLabel.text = book.author;
    _nameLabel.text = book.name;
}

- (IBAction)didTap:(id)sender {
    if (_tapHandler)
        _tapHandler(_book);
}

@end
