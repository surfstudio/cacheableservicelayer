//
//  ServiceRequestConfig.m
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 12/08/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import "ServiceRequestConfig.h"

@implementation ServiceRequestConfig

@end

@implementation CSLRequest (ConcreteConfig)

- (ServiceRequestConfig *)config {
    return self.userInfo;
}

@end
