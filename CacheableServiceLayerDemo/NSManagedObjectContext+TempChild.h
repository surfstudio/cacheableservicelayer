//
//  NSManagedObjectContext+TempChild.h
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 04/07/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import <CoreData/CoreData.h>
#import <MagicalRecord/MagicalRecord.h>

@interface NSManagedObjectContext (TempChild)
- (void)makeTempChildWithHolder:(id)holder block:(void (^)(NSManagedObjectContext *localContext))block;
- (void)makeTempChildWithHolder:(id)holder block:(void (^)(NSManagedObjectContext *localContext))block completion:(void (^)())completion;
@end
