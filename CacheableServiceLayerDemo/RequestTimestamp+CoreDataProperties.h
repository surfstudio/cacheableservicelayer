//
//  RequestTimestamp+CoreDataProperties.h
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 20/02/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "RequestTimestamp.h"

NS_ASSUME_NONNULL_BEGIN

@interface RequestTimestamp (CoreDataProperties)

@property (nullable, nonatomic, retain) NSDate *date;
@property (nullable, nonatomic, retain) NSString *method;
@property (nullable, nonatomic, retain) NSString *parameters;
@property (nullable, nonatomic, retain) NSString *url;
@property (nullable, nonatomic, retain) NSDate *expirationDate;
@property (nullable, nonatomic, retain) NSSet<Book *> *books;

@end

@interface RequestTimestamp (CoreDataGeneratedAccessors)

- (void)addBooksObject:(Book *)value;
- (void)removeBooksObject:(Book *)value;
- (void)addBooks:(NSSet<Book *> *)values;
- (void)removeBooks:(NSSet<Book *> *)values;

@end

NS_ASSUME_NONNULL_END
