//
//  ServiceDelegate.m
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 15/08/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import "ServiceDelegate.h"
#import "ServiceRequestConfig.h"
#import "ErrorMessages.h"

@implementation ServiceDelegate

+ (instancetype)sharedInstance {
    static id res = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        res = [[self alloc] init];
    });
    return res;
}

- (void)csl_requestWasCreated:(CSLRequest *)request {
    request.userInfo = [ServiceRequestConfig new];
}

- (void)csl_requestDidFinish:(CSLRequest *)request {
    [ErrorMessages handleErrorForRequest:request response:request.response];
}

- (void)csl_request:(CSLRequest *)request didReceiveUnhandledResponse:(CSLResponse *)response {
    [ErrorMessages handleErrorForRequest:request response:response];
}

@end
