//
//  A2DynamicDelegate+ManyMethods.m
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 15/01/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import "A2DynamicDelegate+ManyMethods.h"

@implementation A2DynamicDelegate (ManyMethods)

- (instancetype)implementMethodsWithBlocks:(SEL)firstMethod, ... {
    if (!firstMethod) return self;
    va_list args;
    va_start(args, firstMethod);
    SEL method = firstMethod;
    id block = va_arg(args, id);
    if (block) {
        do [self implementMethod:method withBlock:block];
        while ((method = va_arg(args, SEL)) && (block = va_arg(args, id)));
    }
    va_end(args);
    return self;
}

@end


@implementation NSObject (AttachImplementation)

- (id)attachImplementation:(Protocol *)protocol methods:(SEL)firstMethod, ... {
    if (!protocol) return nil;
    A2DynamicDelegate *res = [self bk_dynamicDelegateForProtocol:protocol];
    va_list args;
    va_start(args, firstMethod);
    SEL method = firstMethod;
    id block = va_arg(args, id);
    if (block) {
        do [res implementMethod:method withBlock:block];
        while ((method = va_arg(args, SEL)) && (block = va_arg(args, id)));
    }
    va_end(args);
    return res;
}

@end
