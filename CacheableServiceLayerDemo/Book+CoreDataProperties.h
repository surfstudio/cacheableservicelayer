//
//  Book+CoreDataProperties.h
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 20/02/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Book.h"

NS_ASSUME_NONNULL_BEGIN

@interface Book (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *author;
@property (nullable, nonatomic, retain) NSString *bookDescription;
@property (nullable, nonatomic, retain) NSString *id_;
@property (nullable, nonatomic, retain) NSString *name;
@property (nonatomic) int32_t sortOrder;
@property (nullable, nonatomic, retain) NSSet<RequestTimestamp *> *requests;

@end

@interface Book (CoreDataGeneratedAccessors)

- (void)addRequestsObject:(RequestTimestamp *)value;
- (void)removeRequestsObject:(RequestTimestamp *)value;
- (void)addRequests:(NSSet<RequestTimestamp *> *)values;
- (void)removeRequests:(NSSet<RequestTimestamp *> *)values;

@end

NS_ASSUME_NONNULL_END
