//
//  PropertyName.h
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 15/02/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#define propName(MyClass, myProp) (YES?:(void)[MyClass alloc].myProp, @#myProp)
