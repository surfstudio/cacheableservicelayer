//
//  ServerResponse.h
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 23/12/15.
//  Copyright © 2015 Surf Studio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CacheableServiceLayer/CacheableServiceLayer.h>

@interface ServerResponse : NSObject
@property (nonatomic) NSError *error;
@property (nonatomic) NSDictionary *dict;
@property (nonatomic) NSHTTPURLResponse *httpResponse;

// example of project-specific logic: calculated properties from the returned json
@property (nonatomic, readonly) NSString *serverCode;
@property (nonatomic, readonly) NSString *serverMessage;
@end
