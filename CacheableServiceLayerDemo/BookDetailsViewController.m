//
//  BookDetailsViewController.m
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 25/01/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import "BookDetailsViewController.h"
#import "DetailedBookRequest.h"
#import "EphemeralBookRequest.h"
#import "NSObject+CallMethodsWithPrefix.h"
#import <libextobjc/extobjc.h>
#import <SurfObjcUtils/SurfObjcUtils+UIKit.h>
#import <BlocksKit/BlocksKit+UIKit.h>
#import <Masonry/Masonry.h>

@interface BookDetailsViewController ()
@property (weak, nonatomic) IBOutlet UILabel *authorLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *cannotLoadLabel;
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) IBOutlet UIView *cannotLoadView;
@property (strong, nonatomic) IBOutlet UIView *hudView;
@property (weak, nonatomic) UIView *currentView;
@property (strong, nonatomic) CSLRepeatingRequestController *requestController;
@property (nonatomic, readonly) Book *book;
@end

@implementation BookDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self callMethodsWithPrefix:@"VDL_"];
    [self reloadAllForced:NO];
}

- (void)VDL_configureRequestController {
    _requestController = [CSLRepeatingRequestController new];
    [_requestController setObservedDataMapper:^id(CSLResponse *response) {
        Book *book = response.result;
        return @[book.id_,
                 unnullify(book.name),
                 unnullify(book.author),
                 unnullify(book.bookDescription)];
    }];
    if (!_ephemeralRequests) {
        @weakify(self);
        [_requestController subscribeToCacheAspects:@[BooksAspect] cacheRequestMaker:^CSLRequest *{
            @strongify(self);
            return [[self makeRequest] configureToCheckChangesSynchronously];
        } changeHandler:^(CSLResponse *response) {
            @strongify(self);
            [self refreshUi];
        }];
    }
}

- (void)VDL_configreRefreshControl {
    _scrollView.srf_attachedRefreshControl = [[UIRefreshControl alloc] init];
    @weakify(self);
    [_scrollView.srf_attachedRefreshControl bk_addEventHandler:^(id sender) {
        @strongify(self);
        [self reloadAllForced:YES];
    } forControlEvents:UIControlEventValueChanged];
}

- (CSLRequest *)makeRequest {
    if (_ephemeralRequests) {
        EphemeralBookRequest *request = [EphemeralBookRequest new];
        request.bookId = self.bookId;
        return request;
    } else {
        DetailedBookRequest *request = [DetailedBookRequest new];
        request.bookId = self.bookId;
        return request;
    }
}

- (Book *)book {
    return _requestController.lastSuccessfulResponse.result;
}

- (void)refreshUi {
    UIView *oldCurrentView = _currentView;
    _currentView = ^UIView *{
        if (self.book) {
            return _contentView;
        } else {
            if (_requestController.lastResponse)
                return _cannotLoadView;
            else
                return _hudView;
        }
    }();
    
    //place the corresponding view to scrollView
    if (oldCurrentView != _currentView) {
        [oldCurrentView removeFromSuperview];
        [_scrollView addSubview:_currentView];
        [_currentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(_scrollView);
            make.width.equalTo(_scrollView);
            //everything except contentView will have fixed height
            if (_currentView != _contentView)
                make.height.equalTo(_scrollView);
        }];
    }
    
    Book *book = self.book;
    if (book) {
        _authorLabel.text = book.author;
        _nameLabel.text = book.name;
        _descriptionLabel.text = book.bookDescription.copy;
    }
    
    BOOL unscrollableHud = _currentView == _hudView;
    _scrollView.bounces = !unscrollableHud;
    _scrollView.srf_attachedRefreshControl.srf_disabledInScrollView = unscrollableHud;
    _scrollView.srf_attachedRefreshControl.refreshing = _requestController.currentRequest && !unscrollableHud;

    [self.view layoutIfNeeded];
}

- (void)reloadAllForced:(BOOL)forced {
    if (_requestController.currentRequest) return;
    CSLRequest *request = [self makeRequest];
    request.config.vcToShowError = self;
    @weakify(self);
    [request.config setCustomErrorHandler:^(CSLRequest *request, CSLResponse *response) {
        @strongify(self);
        if (self.requestController.currentRequest && !self.book) {
            self.cannotLoadLabel.text = [ErrorMessages messageForRequest:request response:response];
            [self.cannotLoadLabel layoutIfNeeded];
        } else
            [ErrorMessages showErrorForRequest:request response:response];
    }];
    if (!_ephemeralRequests) {
        ((CSLCacheableRequest *)request).cacheScenario = (forced ?
                                                          CSLCacheScenarioServerOnly:
                                                          CSLCacheScenarioFirstCacheThenRefreshFromServer);
    }
    [_requestController startRequest:request completion:^(CSLResponse *response, BOOL changed) {
        [self refreshUi];
    }];
    [self refreshUi];
}

@end
