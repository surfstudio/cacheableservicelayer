//
//  ErrorMessages.h
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 12/08/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CacheableServiceLayer/CacheableServiceLayer.h>

@interface ErrorMessages : NSObject
+ (void)handleErrorForRequest:(CSLRequest *)request response:(CSLResponse *)response;
+ (void)showErrorForRequest:(CSLRequest *)request response:(CSLResponse *)response;
+ (NSString *)messageForRequest:(CSLRequest *)request response:(CSLResponse *)response;
@end
