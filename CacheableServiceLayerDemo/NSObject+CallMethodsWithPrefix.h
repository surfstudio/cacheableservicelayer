//
//  NSObject+CallMethodsWithPrefix.h
//  okey
//
//  Created by Dmitrii Trofimov on 28/10/15.
//  Copyright © 2015 SurfStudio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (CallMethodsWithPrefix)
-(void)callMethodsWithPrefix:(NSString*)prefix;
@end
