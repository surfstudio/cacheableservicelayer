//
//  NSManagedObject+MRMappingUtils.h
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 15/02/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObject (MRMappingUtils)
+ (NSArray *)MR_importFromArray:(NSArray *)listOfObjectData
           andRemoveOldIfNeeded:(NSArray *)oldObjects
                      inContext:(NSManagedObjectContext *)context;
@end
