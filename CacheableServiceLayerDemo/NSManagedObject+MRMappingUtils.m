//
//  NSManagedObject+MRMappingUtils.m
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 15/02/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import "NSManagedObject+MRMappingUtils.h"
#import <MagicalRecord/MagicalRecord.h>

@implementation NSManagedObject (MRMappingUtils)

+ (NSArray *)MR_importFromArray:(NSArray *)dicts andRemoveOldIfNeeded:(NSArray *)oldObjects inContext:(NSManagedObjectContext *)context {
    NSArray *newObjects = [self MR_importFromArray:dicts inContext:context];
    for (NSManagedObject *oldObj in oldObjects) {
        if (![newObjects containsObject:oldObj]) {
            [oldObj MR_deleteEntityInContext:context];
        }
    }
    return newObjects;
}

@end
