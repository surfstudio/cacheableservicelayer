//
//  UserMessageView.h
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 21/12/15.
//  Copyright © 2015 Surf Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageDisplayManager.h"

@interface UserMessageView : UIView
@property (nonatomic) NSString *text;
@property (nonatomic) MessageDisplayMode mode;
+ (instancetype)view;
@end
