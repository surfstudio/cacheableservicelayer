//
//  RequestTimestamp.m
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 25/12/15.
//  Copyright © 2015 Surf Studio. All rights reserved.
//

#import "RequestTimestamp.h"
#import <MagicalRecord/MagicalRecord.h>
#import "NSPredicate+Templates.h"

@implementation RequestTimestamp

+ (NSPredicate *)expiredPredicate {
    return [NSPredicate predicateWithTemplate:@"expirationDate < $NOW"
                                       params:@{@"NOW":[NSDate date]}];
}

+ (NSPredicate *)unexpiredPredicate {
    return [NSPredicate predicateWithTemplate:@"expirationDate >= $NOW"
                                       params:@{@"NOW":[NSDate date]}];
}

+ (void)removeExpiredInContext:(NSManagedObjectContext *)context {
    [RequestTimestamp MR_deleteAllMatchingPredicate:[self expiredPredicate] inContext:context];
}

@end
