//
//  MagicalRecord+SerialSaving.m
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 15/02/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import "MagicalRecord+SerialSaving.h"

@implementation MagicalRecord (SerialSaving)

+ (void)serialSaveWithBlock:(void (^)(NSManagedObjectContext *))block completion:(MRSaveCompletionHandler)completion {
    static NSManagedObjectContext *localContext = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSManagedObjectContext *savingContext  = [NSManagedObjectContext MR_rootSavingContext];
        localContext = [NSManagedObjectContext MR_contextWithParent:savingContext];
    });
    
    [localContext performBlock:^{
        [localContext MR_setWorkingName:NSStringFromSelector(_cmd)];
        
        if (block) {
            block(localContext);
        }
        
        [localContext MR_saveWithOptions:MRSaveParentContexts completion:completion];
    }];
}

@end
