//
//  MagicalRecord+TempContext.m
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 12/07/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import "MagicalRecord+TempContext.h"
#import "NSManagedObjectContext+TempChild.h"

@implementation MagicalRecord (TempContext)

+ (void)makeTempContextWithHolder:(id)holder block:(void (^)(NSManagedObjectContext *))block {
    [[NSManagedObjectContext MR_rootSavingContext] makeTempChildWithHolder:holder block:block];
}

+ (void)makeTempContextWithHolder:(id)holder block:(void (^)(NSManagedObjectContext *))block completion:(void (^)())completion {
    [[NSManagedObjectContext MR_rootSavingContext] makeTempChildWithHolder:holder block:block completion:completion];
}

@end
