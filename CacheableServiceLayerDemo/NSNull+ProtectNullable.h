//
//  NSNull+ProtectNullable.h
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 15/01/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#define unnullify(obj) obj?:[NSNull null]
