//
//  DeleteBookRequest.h
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 13/01/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import "ServiceUtils.h"
#import "Book+CoreDataProperties.h"

///Delete the specific book.
@interface DeleteBookRequest : CSLRequest
@property (nonatomic) NSString *bookId;
@end
