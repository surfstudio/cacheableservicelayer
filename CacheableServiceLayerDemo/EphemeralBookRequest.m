//
//  EphemeralBookRequest.m
//  CacheableServiceLayer
//
//  Created by Dmitrii Trofimov on 04/07/16.
//  Copyright © 2016 Surf Studio. All rights reserved.
//

#import "EphemeralBookRequest.h"
#import "RequestUtils.h"

@implementation EphemeralBookRequest

- (void)performCustomScenarioWithCompletion:(CSLRequestCompletion)completion {
    ServerRequest *serverRequest = [ServerRequest requestWithMethod:GET relativeUrl:[NSString stringWithFormat:@"books/%@.json",_bookId] parameters:nil];
    [serverRequest performWithCompletion:^(ServerResponse *serverResponse) {
        CSLResponse *response = [CSLResponse responseWithServerResponse:serverResponse];
        if (response.success) {
            NSDictionary *bookDict = serverResponse.dict;
            if (bookDict && [bookDict isKindOfClass:[NSDictionary class]]) {
                __block Book *book = nil;
                [MagicalRecord makeTempContextWithHolder:response block:^(NSManagedObjectContext *context) {
                    book = [Book MR_importFromObject:bookDict inContext:context];
                } completion:^{
                    if (book)
                        response.result = book;
                    else
                        response.error = [NSError csl_invalidResponseError];
                    completion(response);
                }];
            } else
                completion([response addError:[NSError csl_invalidResponseError]]);
        } else
            completion(response);
    }];
}

@end
