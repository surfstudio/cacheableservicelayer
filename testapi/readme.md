# Как запустить этот "апи" у себя в localhost

В терминале идём в папку, где по дефолту лежит контент для встроенного веб-сервера:

	cd /Library/WebServer/Documents

Там создаём символьную ссылку на нашу папку:

	sudo ln -s ~/iOS\ projects/CacheableServiceLayer/testapi/

Запускаем встроенный веб-сервер:

	sudo apachectl start

Открываем в браузере ссылку:

	http://localhost/testapi/books.json

В идеале, должен открыться этот самый json.
